"""
Flask API of the SMS Spam detection model model.
"""
# import traceback
import joblib
from flasgger import Swagger
from flask import Flask, jsonify, request
import requests
import os
import time

from text_preprocessing import _text_process, _extract_message_len, _preprocess, prepare

app = Flask(__name__)
swagger = Swagger(app)
MYWEB_HOST = os.environ.get("MYWEB_HOST")
COLOR = os.environ.get("COLOR")
START_TIME = time.time()
with open("version.txt") as version_file:
    version = version_file.read().strip()


@app.route('/predict', methods=['POST'])
def predict():
    """
    Predict whether an SMS is Spam.
    ---
    consumes:
      - application/json
    parameters:
        - name: input_data
          in: body
          description: message to be classified.
          required: True
          schema:
            type: object
            required: sms
            properties:
                sms:
                    type: string
                    example: This is an example of an SMS.
    responses:
      200:
        description: "The result of the classification: 'spam' or 'ham'."
    """
    input_data = request.get_json()
    sms = input_data.get('sms')
    processed_sms = prepare(sms)
    model = joblib.load('output/model.joblib')
    prediction = model.predict(processed_sms)[0]

    res = {
        "result": prediction,
        "classifier": "decision tree",
        "sms": sms
    }
    print(res)
    return jsonify(res)


@app.route('/dumbpredict', methods=['POST'])
def dumb_predict():
    """
    Predict whether a given SMS is Spam or Ham (dumb model: always predicts 'ham').
    ---
    consumes:
      - application/json
    parameters:
        - name: input_data
          in: body
          description: message to be classified.
          required: True
          schema:
            type: object
            required: sms
            properties:
                sms:
                    type: string
                    example: This is an example of an SMS.
    responses:
      200:
        description: "The result of the classification: 'spam' or 'ham'."
    """
    input_data = request.get_json()
    sms = input_data.get('sms')

    return jsonify({
        "result": "Spam",
        "classifier": "decision tree",
        "sms": sms
    })


@app.route("/health")
def health():
    res = requests.get(f"{MYWEB_HOST}/health")
    proxy_starttime = float(res.text)
    if proxy_starttime > START_TIME:
        return "Restart required", 500
    else:
        return res.text, res.status_code


if __name__ == '__main__':
    clf = joblib.load('output/model.joblib')
    while True:
        try:
            print(f"Attempting to register at {MYWEB_HOST}/register with version {version}")
            resp = requests.post(f"{MYWEB_HOST}/register", json={"version": version,
                                                                 "color": COLOR})
            assert resp.status_code == 200
            break
        except Exception as e:
            print(f"Could not reach myweb service: {e}")
            time.sleep(1)

    app.run(host="0.0.0.0", port=8080, debug=True)
