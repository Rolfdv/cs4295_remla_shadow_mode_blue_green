# cs4295_remla_shadow_mode_blue_green
This project is for the course CS4295 - Release engineering for machine learning applications.
Our project will focus on combining shadow mode and green/blue deployment for automatic deployment of new machine learning models.

This project hosts the code to run pods with ML models used for REMLA group 8's k8s cluster for Blue-Shadow-Green deployment.

## Package dependencies
This project requires [kubectl](https://kubernetes.io/docs/reference/kubectl/kubectl/) to run. [Minikube](https://minikube.sigs.k8s.io/docs/start/) is an excellent way to get started. Install it via the get started page before continuing.

## Other dependencies
This project requires a proxy service in the k8s cluster to copy incoming traffic to both pods. It can be found [here](https://gitlab.com/Rolfdv/proxy_service).

## Initial start
Before starting, make sure the proxy service mentioned under `Other dependencies` runs.
If you want to start with different versioned pods than the default (GREEN: 0.1.2 and BLUE: 0.1.3), edit this in `my-models.yml`. Only those or later versions are advised.

To first start using this service, run `kubectl apply -f my-models.yml`.

## New version
To deploy a new version, add a version tag to your commit of the format `v<MAJOR>.<MINOR>.<PATCH>`. The CI/CD pipeline will create a new docker container from this commit.
Next, update either the BLUE or GREEN pod in `my-models.yml` to target the right version. For example, change `image: registry.gitlab.com/rolfdv/cs4295_remla_shadow_mode_blue_green:0.1.2` to `image: registry.gitlab.com/rolfdv/cs4295_remla_shadow_mode_blue_green:0.1.3` if you want that pod to run version 0.1.3 instead of 0.1.2.

To run this new version with kubectl, run `kubectl apply -f my-models.yml`.

## What is this project for?
This project provides a k8s config file to run two similar pods that host a machine learning application via its REST API. It detects spam in SMS messages currently. One of the pods runs in shadow mode while the other does not, both pods receive the same traffic, but only the response from the non-shadow mode pod is returned to the requester.

The goal of this project is to serve as implementation of our proposed Blue-Shadow-Green deployment strategy.